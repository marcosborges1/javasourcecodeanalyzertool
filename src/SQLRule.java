public class SQLRule {
	
	private Operation operation;
	
	private int ruleNumber;
	
	private String databaseSQL;
	
	private String googleAppEngine;
	
	public SQLRule(Operation operation, int ruleNumber,
			String databaseSQL, String googleAppEngine) {
		super();
		this.operation = operation;
		this.ruleNumber = ruleNumber;
		this.databaseSQL = databaseSQL;
		this.googleAppEngine = googleAppEngine;
	}


	public Operation getOperation() {
		return operation;
	}

	public int getRuleNumber() {
		return ruleNumber;
	}

	public String getDatabaseSQL() {
		return databaseSQL;
	}

	public String getGoogleAppEngine() {
		return googleAppEngine;
	}
	
	@Override
	public String toString() {
		StringBuffer sbf = new StringBuffer();
		sbf.append("\n-------------------------------------------------------");
		sbf.append("\nSQL CRUD Operation Type: "+ operation);
		sbf.append("\nRule No: "+ ruleNumber);
		sbf.append("\nSQL Query: "+ databaseSQL);
		sbf.append("\nDataStore API"+ googleAppEngine);
		sbf.append("\n-------------------------------------------------------");
		return sbf.toString();
	}

}
