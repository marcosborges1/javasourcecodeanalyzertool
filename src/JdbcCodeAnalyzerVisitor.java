import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree.Kind;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;

/**
 * The visitor object implementation that visits the method nodes in the Abstract Syntax Tree(AST) representation of a Java source file.
 * A java source file can be represented as a tree of nodes and each node represent a meaninful construct.
 * 
 * @author Prasanth M P
 *
 */
public class JdbcCodeAnalyzerVisitor extends TreePathScanner<Object, Object> {

	private ProcessingEnvironment processingEnv;

	public JdbcCodeAnalyzerVisitor(ProcessingEnvironment processingEnv) {
		this.processingEnv = processingEnv;
	}
	
	/**
	 * The visitor method invoked when a method declaration is found in the Abstract syntax tree notation of the java source file.
	 */
	@Override
	public Object visitMethod(MethodTree methodTree, Object arg1) {
		Trees trees = (Trees) arg1;
		CompilationUnitTree compilationUnitTree = getCurrentPath().getCompilationUnit();
		// compileTree.getSourceFile().getName(); // filename including path
		List<? extends StatementTree> statements = methodTree.getBody()
				.getStatements();
		processStatements(statements,trees,compilationUnitTree,methodTree);
		return super.visitMethod(methodTree, arg1);
	}

	/**
	 * This method is recursively called to process the occurence of an SQL query string within a method.
	 * 
	 * @param statements - The List of StatementTree nodes. The StatementTree node represent
	 * a statement in the method such as if, else, else if, try, catch, for, finally  etc.
	 * @param trees  - Provides utilities for operations on Abstract Syntax Trees.
	 * @param compileTree - Represents the abstract syntax tree for compilation units (source files).
	 * @param methodTree - Represents a method node within the AST representation of the Java source file.
	 */
	private void processStatements(List<? extends StatementTree> statements, Trees trees,CompilationUnitTree compileTree,
			MethodTree methodTree ) {
		
		for (StatementTree statement : statements) {	
			if (statement.getKind().equals(Kind.EXPRESSION_STATEMENT)) {
				handleMessageForExpressionStatment(trees,statement,compileTree,methodTree);
			} else if (statement.getKind().equals(Kind.VARIABLE)) {
				handleMessageForVariable(trees,statement,compileTree,methodTree);
			} else if(statement.getKind().equals(Kind.TRY) ){
				handleMessageForTryStatement(trees,statement,compileTree,methodTree);
			} else if ( statement.getKind().equals(Kind.IF)){
				handleMessageForIFStatement(trees,statement,compileTree,methodTree);
			} else if(statement.getKind().equals(Kind.RETURN)){
				handleMessageForReturnStatement(trees,statement,compileTree,methodTree);
			} else if(statement.getKind().equals(Kind.ENHANCED_FOR_LOOP)){
				handleMessageForEnhancedForLoop(trees,statement,compileTree,methodTree);
			}
		}
	}

	/**
	 * The method is called when a for loop in encountered in the source file. The method then obtains the statements within the for loop
	 * and recursively calls the processStatements method.
	 */
	private void handleMessageForEnhancedForLoop(Trees trees,
			StatementTree statement, CompilationUnitTree compileTree,
			MethodTree methodTree) {
		EnhancedForLoopTree enhanceForLoop = ( EnhancedForLoopTree)statement;
	    enhanceForLoop.getExpression();
	    List<? extends StatementTree> statements = ((BlockTree)enhanceForLoop.getStatement()).getStatements();
		processStatements(statements, trees, compileTree, methodTree);
	}

	/**
	 * The method is called when a return statement is encountered in the source file. The method then obtains the statements within the return statement
	 * and calls the handleMessageForExpressionStatment to display an error log to indicate if String variable returned is an SQL query.
	 */
	private void handleMessageForReturnStatement(Trees trees,
			StatementTree statement, CompilationUnitTree compileTree,
			MethodTree methodTree) {
		ReturnTree rt = ( ReturnTree)statement;
		if( rt.getExpression().getKind().equals(Kind.METHOD_INVOCATION) ) {
		  MethodInvocationTree methodInvocationTree =  (MethodInvocationTree)rt.getExpression();
			List<? extends ExpressionTree> args =	  methodInvocationTree.getArguments();
			for(ExpressionTree ex: args){
				if(ex.getKind().equals(Kind.STRING_LITERAL)){
					handleMessageForExpressionStatment(trees,statement, compileTree,  methodTree);
				}
			}	 
		}
	}

	/**
	 * The method is called when a if else is encountered in the source file. The method then obtains the statements within the if else 
	 * and calls the processStatements recursively.
	 */
	private void handleMessageForIFStatement(Trees trees,
			StatementTree statement, CompilationUnitTree compileTree,
			MethodTree methodTree) {
	 	IfTree iftree = (IfTree)statement;
	 	BlockTree ifThenBlock=  (BlockTree)iftree.getThenStatement();
	 	ifThenBlock.getStatements();
	 	processStatements(ifThenBlock.getStatements(), trees, compileTree, methodTree);
	 	BlockTree ifElseBlock =  (BlockTree)iftree.getElseStatement();
	 	processStatements(ifElseBlock.getStatements(), trees, compileTree, methodTree);
	}

	/**
	 * The method is called when a Try block is encountered in the source file. The method then obtains the statements to check if the statements within
	 * the Try block is of expression statements or variables and then calls appropriate methods for processing further.
	 */
	private void handleMessageForTryStatement(Trees trees,
			StatementTree statement, CompilationUnitTree compileTree,
			MethodTree methodTree) {
		TryTree tree = (TryTree)statement;
		List<? extends StatementTree> blockStatements = tree.getBlock().getStatements();
		for(StatementTree stmt : blockStatements){
			if (stmt.getKind().equals(Kind.EXPRESSION_STATEMENT)) {
				handleMessageForExpressionStatment(trees,stmt,compileTree,methodTree);
			} else if (stmt.getKind().equals(Kind.VARIABLE)) {
				handleMessageForVariable(trees,stmt,compileTree,methodTree);
			}
		}
	}

	/**
	 * checks if the string values "select ", "update ", "insert ", or "delete " present in the declared variable.
	 */
	private void handleMessageForVariable(Trees trees, StatementTree statement,
			CompilationUnitTree compileTree, MethodTree methodTree) {
		VariableTree queryVariable = (VariableTree) statement;
		SourcePositions sourcePosition = trees.getSourcePositions();
		long startPosition = sourcePosition.getStartPosition(
				getCurrentPath().getCompilationUnit(), statement);
		if (statement.toString() != null && !statement.toString().isEmpty()) {
			if (statement.toString().toLowerCase().contains("select ")) {
				processingEnv
						.getMessager()
						.printMessage(
								javax.tools.Diagnostic.Kind.ERROR,
								"Use  Data Store retrieve entity operations on line "
										+ compileTree.getLineMap()
												.getLineNumber(
														startPosition)
										+ " for the jdbc query string variable "
										+ queryVariable.getName()
										+ " in the method "
										+ methodTree.getName()
										+ " in the class "
										+ TreeUtils.className(trees
												.getElement(getCurrentPath()))
										+ " [ "
										+ compileTree.getSourceFile()
												.getName() + " ] "+" Refer Rules "+  SQLRuleRepository.getRule(Operation.READ) + " in the Rule Table");
			} else if (statement.toString().toLowerCase()
					.contains("update ")) {
				processingEnv
						.getMessager()
						.printMessage(
								javax.tools.Diagnostic.Kind.ERROR,
								"Use  Data Store update entity operations on line "
										+ compileTree.getLineMap()
												.getLineNumber(
														startPosition)
										+ " for the jdbc query string variable "
										+ queryVariable.getName()
										+ " in the method "
										+ methodTree.getName()
										+ " in the class "
										+ TreeUtils.className(trees
												.getElement(getCurrentPath()))
										+ " [ "
										+ compileTree.getSourceFile()
												.getName() + " ] "+" Refer Rules "+  SQLRuleRepository.getRule(Operation.UPDATE) + " in the Rule Table");
			} else if (statement.toString().toLowerCase()
					.contains("insert ")) {
				processingEnv
						.getMessager()
						.printMessage(
								javax.tools.Diagnostic.Kind.ERROR,
								"Use  Data Store create entity operations on line "
										+ compileTree.getLineMap()
												.getLineNumber(
														startPosition)
										+ " for the jdbc query string variable "
										+ queryVariable.getName()
										+ " in the method "
										+ methodTree.getName()
										+ " in the class "
										+ TreeUtils.className(trees
												.getElement(getCurrentPath()))
										+ " [ "
										+ compileTree.getSourceFile()
												.getName() + " ] "+" Refer Rules "+  SQLRuleRepository.getRule(Operation.CREATE) + " in the Rule Table");
			} else if (statement.toString().toLowerCase()
					.contains("delete ")) {
				processingEnv
						.getMessager()
						.printMessage(
								javax.tools.Diagnostic.Kind.ERROR,
								"Use  Data Store delete entity operations on line "
										+ compileTree.getLineMap()
												.getLineNumber(
														startPosition)
										+ " for the jdbc query string variable "
										+ queryVariable.getName()
										+ " in the method "
										+ methodTree.getName()
										+ " in the class "
										+ TreeUtils.className(trees
												.getElement(getCurrentPath()))
										+ " [ "
										+ compileTree.getSourceFile()
												.getName() + " ] "+" Refer Rules "+  SQLRuleRepository.getRule(Operation.DELETE) + " in the Rule Table");
			}
		}
	}

	/**
	 * checks if there is a string "select ", "update ", "insert ", or "delete " in the statement.
	 */
	private void handleMessageForExpressionStatment(Trees trees,StatementTree stmt, CompilationUnitTree compilationUnitTree, MethodTree methodTree) {
		SourcePositions sourcePosition = trees.getSourcePositions();
		long startPosition = sourcePosition.getStartPosition(
				getCurrentPath().getCompilationUnit(), stmt);
		if (stmt.toString() != null && !stmt.toString().isEmpty()) {
			if (stmt.toString().toLowerCase().contains("select ")) {
				processingEnv.getMessager().printMessage(
						javax.tools.Diagnostic.Kind.ERROR,
						"Use  Data Store retrieve entity operations on line "
								+ compilationUnitTree.getLineMap()
										.getLineNumber(startPosition)
								+ " in the method "
								+ methodTree.getName().toString()
								+ " in the class "
								+ TreeUtils.className(trees
										.getElement(getCurrentPath()))
								+ " ["
								+ compilationUnitTree.getSourceFile().getName()
								+ "] "+" Refer Rules "+  SQLRuleRepository.getRule(Operation.READ) + " in the Rule Table");
			} else if (stmt.toString().toLowerCase()
					.contains("update ")) {
				processingEnv.getMessager().printMessage(
						javax.tools.Diagnostic.Kind.ERROR,
						"Use  Data Store update entity operations on line "
								+ compilationUnitTree.getLineMap()
										.getLineNumber(startPosition)
								+ " in the method "
								+ methodTree.getName().toString()
								+ " in the class "
								+ TreeUtils.className(trees
										.getElement(getCurrentPath()))
								+ " ["
								+ compilationUnitTree.getSourceFile().getName()
								+ "] "+" Refer Rules "+  SQLRuleRepository.getRule(Operation.UPDATE) + " in the Rule Table");
			} else if (stmt.toString().toLowerCase()
					.contains("insert ")) {
				processingEnv.getMessager().printMessage(
						javax.tools.Diagnostic.Kind.ERROR,
						"Use  Data Store create entity operations on line "
								+ compilationUnitTree.getLineMap()
										.getLineNumber(startPosition)
								+ "  in the method "
								+ methodTree.getName().toString()
								+ " in the class "
								+ TreeUtils.className(trees
										.getElement(getCurrentPath()))
								+ " ["
								+ compilationUnitTree.getSourceFile().getName()
								+ "] "+" Refer Rules "+  SQLRuleRepository.getRule(Operation.CREATE) + " in the Rule Table");
			} else if (stmt.toString().toLowerCase()
					.contains("delete ")) {
				processingEnv.getMessager().printMessage(
						javax.tools.Diagnostic.Kind.ERROR,
						"Use  Data Store delete entity operations on line "
								+ compilationUnitTree.getLineMap()
										.getLineNumber(startPosition)
								+ "  in the method "
								+ methodTree.getName().toString()
								+ " in the class "
								+ TreeUtils.className(trees
										.getElement(getCurrentPath()))
								+ " ["
								+ compilationUnitTree.getSourceFile().getName()
								+ "] "+" Refer Rules "+  SQLRuleRepository.getRule(Operation.DELETE) + " in the Rule Table");
			}
		}
	}

}
