import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.processing.AbstractProcessor;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

/**
 * The main class for starting the SQL Query Analyzer Program.
 * 
 * @author Prasanth M P
 *
 */
public class SQLQueryAnalyzerMain {

	public static void main(String[] args) {

		// Gets the Java programming language compiler
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		// Get a new instance of the standard file manager implementation
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
		List<File> files = new ArrayList<File>();
		List<String> optionList = new ArrayList<String>();
		optionList.addAll(Arrays.asList(System.getProperty("java.class.path")));		
		files.addAll(FileSearchMain.getProjectFiles(ConfigurationPropertiesFileReader.getProperty("project.jdbc.files.location")));
		SQLRuleRepository.showRules();
		if (files.size() > 0) {
			// Get the list of java file objects
			Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(files);
			// Create the compilation task
			CompilationTask task = compiler.getTask(null, fileManager, null, null, null, compilationUnits);
			// Get the list of annotation processors
			LinkedList<AbstractProcessor> processors = new LinkedList<AbstractProcessor>();
			processors.add(new JdbcDAOCodeAnalyzerProcessor());
			task.setProcessors(processors);
			// Perform the compilation task.
			task.call();			
			try {
				fileManager.close();
			} catch (IOException e) {
				System.out.println(e.getLocalizedMessage());
			}
		} else {
			System.out.println("No valid source files to process. "
					+ "Extiting from the program");
			System.exit(0);
		}
	}

}
