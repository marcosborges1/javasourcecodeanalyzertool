import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Prasanth M P
 *
 */
public class SQLRuleRepository {

	public static List<SQLRule> rulesList = new ArrayList<SQLRule>();

	private SQLRuleRepository(){}
	
	static {
		rulesList
				.add(new SQLRule(
						Operation.CREATE,
						1,
						"INSERT INTO TABLE_NAME (COLUMN1, COLUMN2, COLUMN3,...) VALUES (VALUE1, VALUE2, VALUE3,...);",

						"DatastoreService datastore = DatastoreServiceFactory.getDatastoreService(); \n"
								+ "Entity entity = new Entity (\"EntityName\"); \n"
								+ "entity.setProperty(\"propertyName1\", value1);\n"
								+ "entity.setProperty(\"propertyName2\", value2);\n"
								+ "entity.setProperty(\".............\", .......);\n"
								+ "entity.setProperty(\"property...N-1\", valueN-1);\n"
								+ "entity.setProperty(\"property...N\", valueN);\n"
								+ "datastore.put(entity);\n"));

		rulesList
				.add(new SQLRule(
						Operation.CREATE,
						2,
						"PreparedStatement psmt = conn.prepareStatement (\"INSERT INTO TABLE_NAME (COLUMN1, COLUMN2) VALUES (?,?)\");\n"
								+ "psmt.setInt(1, value1);\n"
								+ "psmt.setString(2, value2);\n"
								+ "psmt.addBatch ();\n"
								+ "psmt.setInt (1, value1);\n"
								+ "psmt.setString (2, value2);\n"
								+ "pstmt.addBatch ();\n"
								+ "int[] updateCounts = pstmt.executeBatch();\n",

						"DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();\n"
								+ "Entity entity1 = new Entity(\"Employee\");\n"
								+ "entity1.setProperty(\"propertyName1\", value1);\n"
								+ "entity1.setProperty(\"propertyName2\", value2);\n"
								+ "Entity entity2 = new Entity(\"Employee\");\n"
								+ "Entity2.setProperty(\"propertyName1\", value1);\n"
								+ "entity2.setProperty(\"propertyName2\", value2);\n"
								+ "List<Entity> entityList = Arrays.asList(entity1, entity2);\n"
								+ "datastore.put(entityList);\n"));

		rulesList
				.add(new SQLRule(
						Operation.READ,
						3,
						"SELECT * FROM TABLE_NAME;\n",
						"DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();\n"
								+

								"// Use class Query to assemble a query\n"
								+ "Query q = new Query(\"EntityName\");\n"
								+

								"// Use PreparedQuery interface to retrieve results"
								+ "PreparedQuery pq = datastore.prepare(q);\n"
								+ "// Iterate through the entity list\n"
								+ "for (Entity result : pq.asIterable()) {\n"
								+ "  String value1 = (String)result.getProperty(\"propertyName1\");\n"
								+ "  String value2 = (String)result.getProperty(\"propertyName2\");\n"
								+ "  Long   value3 = (Long)  result.getProperty(\"propertyName3\");\n"
								+ "}"));

		rulesList
				.add(new SQLRule(
						Operation.READ,
						4,
						"SELECT * FROM TABLE_NAME WHERE COLUMN_NAME OPERATOR VALUE;\n",
						"DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();\n"
								+ "// Use class Query to assemble a query\n"
								+ "Query q = new Query(\"EntityName\");\n"
								+ "Query q = new Query(\"EntityName\").addFilter(\"propertyName\",Query.FilterOperator,value);\n"
								+ "// Use PreparedQuery interface to retrieve results\n"
								+ "PreparedQuery pq = datastore.prepare(q);\n"
								+ "// Iterate through the entity list\n"
								+ "for (Entity result : pq.asIterable()) {\n"
								+ "  String value1 = (String)result.getProperty(\"propertyName1\");\n"
								+ "  String value2 = (String)result.getProperty(\"propertyName2\");\n"
								+ "  Long   value3 = (Long)  result.getProperty(\"propertyName3\");\n"
								+ "}"));

		rulesList
				.add(new SQLRule(
						Operation.READ,
						5,
						"SELECT * FROM TABLE_NAME WHERE COLUMN_NAME BETWEEN VALUE1 AND VALUE2;\n",
						"DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();\n"
								+ "Filter rangeFilterCriteria1 = new FilterPredicate(\"propertyName1\",FilterOperator.GREATER_THAN_OR_EQUAL, value1);\n"
								+ "Filter rangeFilterCriteria2 = new FilterPredicate(\"propertyName2\",FilterOperator.LESS_THAN_OR_EQUAL, value2);\n"
								+

								"//Use CompositeFilter to combine multiple filters\n"
								+ "Filter rangeFilter = CompositeFilterOperator.and(rangeFilterCriteria1, rangeFilterCriteria2);\n"
								+

								"// Use class Query to assemble a query\n"
								+ "Query q = new Query(\"EntityName\").setFilter(rangeFilter);\n"
								+ "// Use PreparedQuery interface to retrieve results\n"
								+ "PreparedQuery pq = datastore.prepare(q);\n"
								+ "// Iterate through the entity list\n"
								+ "for (Entity result : pq.asIterable()) {\n"
								+ "  String value1 = (String)result.getProperty(\"propertyName1\");\n"
								+ "  String value2 = (String)result.getProperty(\"propertyName2\");\n"
								+ "  Long   value3 = (Long)  result.getProperty(\"propertyName3\");\n"
								+ "}\n"));

		rulesList
				.add(new SQLRule(
						Operation.UPDATE,
						6,
						"UPDATE TABLE_NAME SET COLUMN1=VALUE1,COLUMN2=VALUE2,.... WHERE SOME_COLUMN=SOME_VALUE;\n",
						"DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();\n"
								+ "// load an existing entity from the datastore\n"
								+ "Entity entity = ... ( Loaded from the DataStore)\n"
								+ "entity.setProperty(\"propertyName1\", newValue1);\n"
								+ "entity.setProperty(\"propertyName2\", newValue2);\n"
								+ "// update the entity with the new property values.\n"
								+ "datastore.put(entity);\n"));

		rulesList
				.add(new SQLRule(
						Operation.DELETE,
						7,
						"DELETE FROM TABLE_NAME WHERE COLUMN_NAME=VALUE;",
						"DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();\n"
								+ "// load an existing entity from the datastore\n"
								+ "Entity entity ........\n"
								+ "// update the entity with the new property values.\n"
								+ "datastore.delete(entity.getKey());\n"));

	}

	public static String getRule(Operation operation) {
		StringBuffer sbf = new StringBuffer("");
		sbf.append(" [ ");
		for (SQLRule sqlRule : rulesList) {
			if (sqlRule.getOperation().equals(operation)) {
				sbf.append(sqlRule.getRuleNumber() + ",");
			}
		}
		sbf.append(" ] ");
		return sbf.toString();
	}

	public static void showRules() {
		for(SQLRule sqlRule : rulesList){
			System.out.println(sqlRule);
		}
	}

}
