import java.io.FileOutputStream;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.Name;


public class GoogleAppEngineBlackList {
	
	private GoogleAppEngineBlackList(){}
	
	// public static List<Class> blackList = new ArrayList<Class>();
	
	public static List<BlackListedClass> blackList = new ArrayList<BlackListedClass>();
	
	
	static {
		
		blackList.add( new BlackListedClass( FileWriter.class,"Please save the file to a Blob store using Blob Store API's"));
		blackList.add( new BlackListedClass( FileOutputStream.class,"Please save the file to a Blob store using Blob Store API's"));
		blackList.add( new BlackListedClass( InitialContext.class,"Google App Engine will not support JNDI API's"));
		blackList.add( new BlackListedClass( Context.class,"Google App Engine will not support JNDI API's"));
		blackList.add( new BlackListedClass( Name.class,"Google App Engine will not support JNDI API's"));
		
		blackList.add( new BlackListedClass( Connection.class,"Google App Engine will not support JDBC Connection class. \n "
				+ "Please use Datastore API's to get a connection to Datastore [DatastoreService datastore = DatastoreServiceFactory.getDatastoreService()]"));
		blackList.add( new BlackListedClass( PreparedStatement.class,"Google App Engine will not support JDBC PreparedStatement.class API's. \n Please use Datastore API's to perform CRUD Operation"));
		blackList.add( new BlackListedClass( Statement.class,"Google App Engine will not support JDBC Statement.class API's. \n Please use Datastore API's to perform CRUD Operation"));
		blackList.add( new BlackListedClass( ResultSet.class,"Google App Engine will not support JDBC ResultSet.class API's. Please use Datastore API's \n"));
		
		blackList.add(new BlackListedClass(System.class, "Features of the java.lang.System class that do not apply to App Engine are disabled. \n"));
		blackList.add(new BlackListedClass(Thread.class, "There are restrictions in using threads please refer google app engine documentation if u recieve any errors \n"));
		blackList.add(new BlackListedClass(Executors.class, "There are restrictions in using threads please refer google app engine documentation if u recieve any errors \n"));
	}
	
	public static List<Class> getBlackList(){
		List<Class> clazzList = new ArrayList<Class>();
		for(BlackListedClass blackListedClass: blackList){
			clazzList.add(blackListedClass.getClazz());
		}
		return clazzList;
	}
	
	public static String getRule(Class clazz){
		for(BlackListedClass blackListedClass: blackList){
			if( blackListedClass.getClazz().equals(clazz) || blackListedClass.getClazz().isAssignableFrom(clazz)){
				return blackListedClass.getRule();
			}
		}
		return "rule not found";
	}
	
	public static boolean contains(Class clazz){
		for(BlackListedClass blackListedClass: blackList){
			if( blackListedClass.getClazz().equals(clazz) ){
				return true;
			}
		}
		return false;
	}

}
