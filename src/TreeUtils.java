import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;

public class TreeUtils {

	private TreeUtils() {
	}

	public static String methodName(Element element) {
		Element e = element.getEnclosingElement();
		while(true ){
			
			if(e.getKind().equals(ElementKind.METHOD)){
				return e.toString();
			} else {
				e = e.getEnclosingElement();
			}
			
		}
	}

	public static String className(Element element) {
		
		Element e = element.getEnclosingElement();
		while(true ){
			if(e.getKind().equals(ElementKind.CLASS)){
				return e.toString();
			} else {
				e = e.getEnclosingElement();
			}
		}
	}
	
	
}
