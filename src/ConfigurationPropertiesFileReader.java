import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigurationPropertiesFileReader {

	private ConfigurationPropertiesFileReader() {}
	
	static Properties defaultProps = null;
	
	static {
		try {
			// create and load default properties
			defaultProps = new Properties();
			FileInputStream in = new FileInputStream(
					"CodeAnalyzerConfiguration.properties");
			defaultProps.load(in);
			in.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String getProperty(String key){
		return defaultProps.getProperty(key);
	}

}
