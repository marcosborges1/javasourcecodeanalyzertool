import java.net.URLClassLoader;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.code.Symbol.VarSymbol;

/**
 * The visitor object implementation that visits the class and method nodes in
 * the Abstract Syntax Tree(AST) representation of a Java source file. A java
 * source file can be represented as a tree of nodes and each node represent a
 * meaninful construct.
 * 
 * @author Prasanth M P
 */
public class BlackListCodeAnalyzerVisitor extends
		TreePathScanner<Object, Object> {

	private ProcessingEnvironment processingEnv;

	private URLClassLoader urlClassLoader;

	public BlackListCodeAnalyzerVisitor(ProcessingEnvironment processingEnv) {
		this.processingEnv = processingEnv;
		this.urlClassLoader = AppEngineAnalyzerUtils.getUrlClassLoader(ConfigurationPropertiesFileReader.getProperty("project.rootfolder.location"));
	}

	/**
	 * The visitor method is invoke when a class declaration is found in the
	 * Java file. Within the method it invoke methods that checks if the class
	 * extends or implements an GAE unsupported JRE Class.
	 */
	@Override
	public Object visitClass(ClassTree node, Object arg1) {
		Trees trees = (Trees) arg1;
		Element element = trees.getElement(getCurrentPath());
		if (AppEngineAnalyzerUtils.isClass(element)) {
			TypeElement clazz = (TypeElement) element;
			// check for JRE super class which is blacklisted
			checkIfSuperClassBlackListed(clazz, urlClassLoader);
			// check if it implements restricted interfaces
			checkIfInterfaceBlackListed(clazz, urlClassLoader);
			// check if it has restricted field variables
		}
		return super.visitClass(node, arg1);
	}

	/**
	 * The visitor method is invoked when a variable declaration is found in the
	 * Java class as a field variable or method variable. The method also checks 
	 * if the variable type is GAE unsupported JRE class.
	 */
	@Override
	public Object visitVariable(VariableTree node, Object p) {
		try {
			JavacTrees javacTrees = (JavacTrees) p;
			Element element = javacTrees.getElement(getCurrentPath());
			VarSymbol s = (VarSymbol) element;
			TreeUtils.className(javacTrees.getElement(getCurrentPath()));
			SourcePositions sourcePositions = javacTrees.getSourcePositions();
			long startPosition = sourcePositions.getStartPosition(
					getCurrentPath().getCompilationUnit(), node);
			javacTrees.getTree(element);
			Class declaredType = urlClassLoader
					.loadClass(s.asType().toString());
			for (Class blackListedClazz : GoogleAppEngineBlackList
					.getBlackList()) {
				if (blackListedClazz.isAssignableFrom(declaredType)) {
					processingEnv
						.getMessager()
						.printMessage(
								javax.tools.Diagnostic.Kind.ERROR,
								"The Class "
										+ TreeUtils.className(javacTrees
												.getElement(getCurrentPath()))
										+ "  has a declared type which is an app engine  restricted JRE Interface/class "
										+ declaredType
										+ " at line number "
										+ getCurrentPath()
												.getCompilationUnit()
												.getLineMap()
												.getLineNumber(
														startPosition)
										+ "\n Rule: "
										+ GoogleAppEngineBlackList
												.getRule(declaredType));
				}
			}
			// }
		} catch (Exception e) {
		}
		return super.visitVariable(node, p);
	}

	/**
	 * The method has the logic to check if a Java class implements an interface class which is a GAE unsupported JRE class. 
	 */
	private void checkIfInterfaceBlackListed(TypeElement clazz,
			URLClassLoader urlClassLoader) {
		List<? extends TypeMirror> interfaces = clazz.getInterfaces();
		for (TypeMirror typeMirror : interfaces) {
			try {
				Class interfaceClazz = urlClassLoader.loadClass(typeMirror
						.toString());
				for (Class blackListedClazz : GoogleAppEngineBlackList
						.getBlackList()) {
					if (blackListedClazz.isAssignableFrom(interfaceClazz)) {
						processingEnv
							.getMessager()
							.printMessage(
									javax.tools.Diagnostic.Kind.ERROR,
									"The Class "
											+ clazz
											+ " implements an app engine restricted JRE Interface "
											+ interfaceClazz
											+ "\n Rule: "
											+ GoogleAppEngineBlackList
													.getRule(interfaceClazz));
					}
				}
			} catch (ClassNotFoundException e) {
			}
		}
	}

	/**
	 * The method has the logic to check if a Java class implements an JRE class which is a GAE unsupported JRE class. 
	 */
	private void checkIfSuperClassBlackListed(TypeElement clazz,
			URLClassLoader urlClassLoader) {
		try {
			Class elementSuperClass = urlClassLoader.loadClass(clazz
					.getSuperclass().toString());
			for (Class blackListedClass : GoogleAppEngineBlackList
					.getBlackList()) {
				if (blackListedClass.isAssignableFrom(elementSuperClass)) {
					processingEnv
						.getMessager()
						.printMessage(
								javax.tools.Diagnostic.Kind.ERROR,
								"The Class "
										+ clazz
										+ " extends an app engine restricted JRE Class "
										+ blackListedClass
										+ "\n Rule: "
										+ GoogleAppEngineBlackList
												.getRule(elementSuperClass));
					return;
				}
			}

		} catch (ClassNotFoundException e) {}
	}

}
