import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import javax.lang.model.element.Element;

/**
 * 
 * @author Prasanth M P
 *
 */
public class AppEngineAnalyzerUtils {

	private AppEngineAnalyzerUtils(){}
	
	public static URLClassLoader getUrlClassLoader(String directoryPath) {
		// directoryPath
		File f = new File(directoryPath);
		URL url;
		URL[] urls = null;
		URLClassLoader urlClassLoader = null;

		try {
			url = f.toURI().toURL();
			urls = new URL[] { url };
			urlClassLoader = new URLClassLoader(urls);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return urlClassLoader;
	}

	public static boolean isClass(Element e) {
		return e.getKind().isClass() || e.getKind().isInterface();
	}
	
}
