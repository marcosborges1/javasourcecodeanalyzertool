public class BlackListedClass {
	
	private Class<?> clazz;
	
	private String rule;

	public BlackListedClass(Class<?> clazz, String rule){
		this.clazz = clazz;
		this.rule = rule;
	}
	
	public Class<?> getClazz() {
		return clazz;
	}

	public String getRule() {
		return rule;
	}
	
}
