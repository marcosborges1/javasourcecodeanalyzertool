import java.net.URLClassLoader;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic.Kind;

@SupportedAnnotationTypes("*")
public class JavaClassCodeAnalyzerProcessor extends AbstractProcessor {

	@Override
	public boolean process(Set<? extends TypeElement> annotations,
			RoundEnvironment roundEnv) {
		//FileSearchMain.getProjectFiles("")
		
		
		URLClassLoader urlClassLoader = AppEngineAnalyzerUtils.getUrlClassLoader(ConfigurationPropertiesFileReader.getProperty("project.rootfolder.location"));
		//URLClassLoader urlClassLoader = AppEngineAnalyzerUtils.getUrlClassLoader("E:/NCIRL/dissertation/workspace/AppEngineBlackListAnalyzer/src/main/java/Counter.java");
		
		for (Element element : roundEnv.getRootElements()) {
			
			if (AppEngineAnalyzerUtils.isClass( element)) {
				TypeElement clazz = (TypeElement) element;

				// check for JRE super class which is blacklisted
				checkIfSuperClassBlackListed(clazz, urlClassLoader);
				// check if it implements restricted interfaces
				checkIfInterfaceBlackListed(clazz, urlClassLoader);
				// check if it has restricted field variables
				checkIfVariablesBlackListed(clazz, urlClassLoader);
			}
		}

		return false;
	}

	private void checkIfVariablesBlackListed(TypeElement clazz,
			URLClassLoader urlClassLoader) {
	
		
		List<? extends Element> enclosingElements = clazz.getEnclosedElements();
		try {
		for(Element element : enclosingElements){
			
			if(element.asType().getKind() == TypeKind.DECLARED){
					
				TypeMirror typeMirror =  element.asType();
					
					Class declaredType = urlClassLoader.loadClass(typeMirror.toString());
					
					if (GoogleAppEngineBlackList.blackList.contains(declaredType)) {
						processingEnv
								.getMessager()
								.printMessage(
										Kind.ERROR,
										"The Class "
												+ clazz
												+ "  has a declared type which is an app engine  restricted JRE Interface/class "
												+ declaredType);
					} else {
						
						for(Class blackListedClazz: GoogleAppEngineBlackList.getBlackList()){
							if(blackListedClazz.isAssignableFrom(declaredType) ){
								
								processingEnv
								.getMessager()
								.printMessage(
										Kind.ERROR,
										"The Class "
												+ clazz
												+ "  has a declared type which is an app engine  restricted JRE Interface/class "
												+ declaredType);
								
							}
						}
					}
				}
			
			if( element.asType().getKind() == TypeKind.EXECUTABLE){
				ExecutableElement executableElement = (ExecutableElement) element;
				
				TypeMirror typeMirrorReturnType = executableElement.getReturnType();
				if( typeMirrorReturnType.getKind() == TypeKind.DECLARED ){
					
					Class declaredType = urlClassLoader.loadClass(typeMirrorReturnType.toString());
					if (GoogleAppEngineBlackList.blackList.contains(declaredType)) {
						processingEnv
								.getMessager()
								.printMessage(
										Kind.ERROR,
										"The Class "
												+ clazz
												+ "  has a method "+ executableElement +" which returns a google app engine restricted interface/class"
												+ declaredType);
					}
				}
				
				List<? extends VariableElement>  methodParameters = executableElement.getParameters();
				for( VariableElement methodParameter :methodParameters ){
					if( methodParameter.asType().getKind() == TypeKind.DECLARED ){
						Class declaredType = urlClassLoader.loadClass(methodParameter.asType().toString());
						if (GoogleAppEngineBlackList.blackList.contains(declaredType)) {
							processingEnv
									.getMessager()
									.printMessage(
											Kind.ERROR,
											"The Class "
													+ clazz
													+ "  has a method "+ executableElement +" which contains a google app engine restricted interface/class as method parameter"
													+ declaredType);
						}
					}
				}
				
			}
			
			
		}
		
		} catch (ClassNotFoundException e) {
			
		}
	}

	private void checkIfInterfaceBlackListed(TypeElement clazz , URLClassLoader urlClassLoader) {
		List<? extends TypeMirror> interfaces = clazz.getInterfaces();
		for (TypeMirror typeMirror : interfaces) {
			
			try {
				
				Class interfaceClazz = urlClassLoader.loadClass(typeMirror.toString());
				if (GoogleAppEngineBlackList.blackList.contains(interfaceClazz)) {
					processingEnv
							.getMessager()
							.printMessage(
									Kind.ERROR,
									"The Class "
											+ clazz
											+ " implements an app engine restricted JRE Interface "
											+ interfaceClazz);
				}

			} catch (ClassNotFoundException e) {
			
			}
		}
	}

	private void checkIfSuperClassBlackListed(TypeElement clazz, URLClassLoader urlClassLoader) {
		try {
			Class elementSuperClass = urlClassLoader.loadClass(clazz.getSuperclass().toString());
			for (Class blackListedClass : GoogleAppEngineBlackList.getBlackList()) {
				if (blackListedClass.isAssignableFrom(elementSuperClass)) {
					processingEnv
							.getMessager()
							.printMessage(
									Kind.ERROR,
									"The Class "
											+ clazz
											+ " extends an app engine restricted JRE Class "
											+ blackListedClass);
					return;
				}
			}

		} catch (ClassNotFoundException e) {
			
		}
	}

	

}
