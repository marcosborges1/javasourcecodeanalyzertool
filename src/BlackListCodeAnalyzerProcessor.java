import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;

/**
 * The annotation processor class for the GAE BlackList Analyzer Program.
 * 
 * @author Prasanth M P.
 */
@SupportedAnnotationTypes("*")
public class BlackListCodeAnalyzerProcessor extends AbstractProcessor {

	private Trees tree;

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		tree = Trees.instance(processingEnv);
	}

	/**
	 * The compiler invokes the process method for processing annotations in a
	 * series of rounds. The annotation processing tool framework will provide
	 * an annotation processor with an object implementing ProcessingEnvironment
	 * interface so the processor can use facilities provided by the framework
	 * to write new files, report error messages, and find other utilities. The
	 * object implementing ProcessingEnvironment interface is then passed to the
	 * Visitor object of class BlackListCodeAnalyzerVisitor via constructor. The
	 * BlackListCodeAnalyzerVisitor can use the object implementing
	 * ProcessingEnvironment to report error messages. 
	 */
	@Override
	public boolean process(Set<? extends TypeElement> annotations,
			RoundEnvironment roundEnv) {
		BlackListCodeAnalyzerVisitor visitor = new BlackListCodeAnalyzerVisitor(
				processingEnv);
		// returns the Java classes, interfaces and packages in the compilation unit as a Set containing Element objects.
		Set<? extends Element> elements = roundEnv.getRootElements();  
		for (Element element : elements) {
			//Gets the TreePath node for a given Element. The TreePath consist of a tree of nodes(Class nodes, method nodes, variable nodes etc) 
			TreePath treePath = tree.getPath(element);		
			if (treePath != null) {
				visitor.scan(treePath, tree);
			}
		}
		return false;
	}
	
}
