import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class returns the list of all java source files in the project folder.
 * 
 * @author Prasanth M P
 *
 */
public class FileSearchMain {

	private FileSearchMain() {
	}

	public static List<File> getProjectFiles(String projectRootFolder) {
		if (projectRootFolder == null) {
			throw new NullPointerException(
					"Please Input a Valid Project Folder for searching App Engine Black listed Classes");
		}
		List<File> fileList = new ArrayList<File>();
		listfiles(projectRootFolder, fileList);
		return fileList;
	}

	public static void listfiles(String directoryName, List<File> files) {
		File directory = new File(directoryName);
		// get all the files from a directory
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile() && file.getName().endsWith(".java")) {
				files.add(file);
			} else if (file.isDirectory()) {
				listfiles(file.getAbsolutePath(), files);
			}
		}

	}

}
